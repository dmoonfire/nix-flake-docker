FROM alpine
ENV USER="root"
ENV PATH="$PATH:/nix/var/nix/profiles/per-user/root/profile/bin:/nix/var/nix/profiles/default/bin"
RUN apk add curl xz tar
RUN addgroup -g 30000 -S nixbld\
  && for i in $(seq 1 30); do adduser -S -D -h /var/empty -g "Nix build user $i" -u $((30000 + i)) -G nixbld nixbld$i ; done\
  && mkdir -m 0755 /etc/nix\
  && mkdir -m 0755 /nix && chown root /nix\
  && echo 'sandbox = false' > /etc/nix/nix.conf\
  && curl -L https://github.com/numtide/nix-unstable-installer/releases/download/nix-2.5pre20211026_5667822/install | sh\
  && mkdir -p /root/.config/nix && echo "experimental-features = nix-command flakes" > /root/.config/nix/nix.conf
RUN nix-env -iA nixpkgs.git nixpkgs.git-lfs nixpkgs.nixFlakes
